<?php

namespace Tickbox\Filters;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Query\Builder;

use Tickbox\Filters\Filter\Parser;

class FiltersServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('tickbox.parser', function ($app) {
            return new Parser();
        });

        Builder::macro('filter', function($class) {
            $filter = app()->make($class);

            $filter->apply($this);
        });
    }
}
