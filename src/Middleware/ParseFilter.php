<?php

namespace Tickbox\Filters\Middleware;

use Closure;

class ParseFilter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        app()->make('tickbox.parser')
             ->parse($request->input('filter'));

        return $next($request);
    }
}
