<?php

namespace Tickbox\Filters\Facades;

use Illuminate\Support\Facades\Facade;

class Filters extends Facade
{
    protected static function getFacadeAccessor() { return 'tickbox.parser'; }
}