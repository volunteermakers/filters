<?php

namespace Tickbox\Filters\Filter;

use Illuminate\Database\Query\Builder;

use Tickbox\Filters\Filter\Item;
use Tickbox\Filters\Filter\Service;

abstract class Set
{
    private $items = [];
    private $parser;

    public function __construct()
    {
        $this->parser = app()->make('tickbox.parser');

        foreach($this->filters() as $key => $class) {
            if ($this->parser->has($key)) {
                $item = app()->make($class);
                $item->value($this->parser->filter($key));

                $this->add($item, $key);
            }
        }
    }

    abstract public function filters();

    public function has($key)
    {
        return !empty($this->items[$key]);
    }

    public function item($key)
    {
        if ($this->has($key)) {
            return $this->items[$key];
        }
    }

    public function all()
    {
        return $this->items;
    }

    public function add(Item $item, $key)
    {
        $this->items[$key] = $item;
    }

    public function apply(Builder $query)
    {
        foreach($this->items as $item) {
            if ($item->value() !== null) {
                $item->apply($query);
            }
        }

        return $this;
    }
}