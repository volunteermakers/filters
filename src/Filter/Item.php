<?php

namespace Tickbox\Filters\Filter;

use Illuminate\Database\Query\Builder;

use Tickbox\Filters\Exceptions\FiltersNoKeyException;
use Tickbox\Filters\Exceptions\FiltersNoColumnException;

abstract class Item
{
    private $value;

    protected $default;

    abstract public function apply(Builder $query);

    public function value($value = null)
    {
        if ($value !== null) {
            $this->value = $value;
        }

        return $this->value !== null ? $this->value : $this->default;
    }

    protected function joined(Builder $query, $table)
    {
        // TODO: need better way to autojoin tables
        foreach($query->joins as $join) {
            if ($join->table == $table) {
                return true;
            }
        }

        return false;
    }
}