<?php

namespace Tickbox\Filters\Filter;

class Parser
{
    private $filters = [];

    public function parse($input)
    {
        foreach(explode(',', $input) as $line) {
            if($line) {
                list($key, $value) = explode(':', $line);
                $this->filters[$key] = $value;
            }
        }
        return $this;
    }

    public function all()
    {
        return $this->filters;
    }

    public function has($key)
    {
        return isset($this->filters[$key]);
    }

    public function filter($key)
    {
        return $this->filters[$key];
    }
}